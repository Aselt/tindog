# Tindog

I've used **bootstrap** and **CSS** to create this beautiful startup landing page. It has a title section as well as feature's sections, a carousel to browse testimonials and pricing plan section. And a call to action where you get your users to download the app, and finally a nice little footer with your socials. Here at **_Tindog_** I code to be mobile responsive using a **Media Query**.

You can see this page [here](https://aselt.gitlab.io/tindog/)

Git commands to download this project:
```
git clone https://gitlab.com/Aselt/tindog.git or SSH here
cd NAME-OF-PROJECT
```
